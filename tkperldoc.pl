#!/usr/bin/perl


# browser for perldoc pages
# 
# grep references to other pages, e.g: CGI::Carp
# and tag it and bind it to a mouse press to open a new window
#
# not intelligent but works OK in nearly all cases

my $pattern = qr/(\bperl\w\w+\b|\b(\w+::\w+(::\w+)*)+\b)|\w+\(\)/i; # looks like perldoc e:g 'perltoot' CGI::Carp
my $man = "perldoc -tT";
use Env;
# $ENV{PERLDOCPAGER} = "/usr/bin/cat"; # we don't want a pager!

use warnings;
use strict;
use Tk;

sub do_perldoc;

sub button_pressed {
    my $text = shift;
    my $name = shift;
    print "look up:$name\n";
    $name =~ $pattern;
    do_perldoc($name);
}


my $page = 0;
my $mw = MainWindow->new;
my $toolbar = $mw->Frame->pack(qw[-expand 0 -fill x]);
$toolbar->Button( -text    => 'Quit', -command => sub { exit },)->pack( qw [-side left]) ;
my $BOOK = $mw->NoteBook(qw[-focus red -backpagecolor snow]); #
$BOOK->pack(qw[-expand 1 -fill both]);
# $f->bind('<Key>', sub { print "arse"}) ;

sub do_escape {

	my $n = $BOOK->info('active');
	exit if ! defined $n;
	print STDERR "$n active\n";
	$BOOK->delete($n);
}
$mw->bind('all', '<Escape>' =>  \&do_escape );

sub do_perldoc {

    my $message = "Hello";
    my $search = shift or die "Search?";
    my $command = "$man ";
    print "\nLook up for 'perltoc' may take a while\n\n" if (lc($search) eq 'perltoc');

    $command .= '-f ' if $search =~ s/\(\)//;
    $command .= $search;


    my $tab = $BOOK->add( ++$page, -label => $search);
    #$tab->Frame()->pack(qw#-expand 1 -fill both#);
    # $tab->pack(qw#-expand 1 -fill both#);
    print "tab=$tab\n";
    $BOOK->raise($page);
    $BOOK->bind('<Q>' => sub { print "$BOOK->delete($page)\n" }  );

    # my $F = $tab->Frame( -background => 'snow' );
    my $b = $tab->Button( -text => "dismiss" , -command => sub {$tab->destroy()} );
    my $msg = $tab->Label(  -relief  => 'sunken',
    			  -justify => 'left',
			  -textvariable  => \$message );

    my $input = $tab->Entry();
    # my $grep = $mw->Label( -text => "Control-F to search");
    my $buffer = $tab->Scrolled( qw/Text -relief sunken
				-selectbackground yellow
				-selectforeground black
				-borderwidth 2 -setgrid true
				-scrollbars e
				-font -adobe-courier-medium-r-normal--14-100-100-100-m-90-iso10646-1
				/);

    print ("buffer is a $buffer\n");
    $tab->bind('<Control-f>', sub {$buffer->FindPopUp()});



    $msg->pack(qw/-expand 0  -fill x -side top/ );

    $buffer->pack(qw/-expand 1 -fill both/ );
    $input->pack(qw/-side left -fill x/);
    $b->pack(-side => 'right');
    $input->bind('<Return>' => sub {do_perldoc($input->get());} );
    $input->bind('<Delete>', sub {$BOOK->delete($page)});

    print "$command\n";
    $message = sprintf "$command";
    $input->delete('0', 'end');
    $input->insert('0', $search);

    open MANPAGE, "2>&1 $command |";
    while (<MANPAGE>) {
	local $\ = "\n";

            # we have to allow for more than 1 tag on a line,
            # so take the 'pos()' of the search, insert text
            # up to that point and continue
	    my ($pos, $len, $tagname);
	    while (m/$pattern/og) {   # search for references

		$tagname = $&;         
		$pos = pos($_);
		$len = length $&;

		$buffer->insert('end', substr($_, 0, $pos-$len));	# ordinary text
		$buffer->insert('end', "$tagname", $tagname);		# tag text
		$buffer->tagConfigure($tagname, qw/-foreground blue/);
		$buffer->tag('bind', $tagname, '<1>' => [ \&button_pressed, $tagname ]);
		# print "tagged $tagname\n";

		$_ = substr($_, $pos); # remove that already inserted
	    } 
	    $buffer->insert('end', $_) if $_; # any left over after search?
	}
	$buffer->configure(qw/-state disabled/); # turn off rw
}


@ARGV = ('perldoc' ) unless @ARGV;

do_perldoc(@ARGV);

MainLoop;
